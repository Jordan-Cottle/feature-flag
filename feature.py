""" A feature flag decorator that links configuration input to function calls."""

from typing import OrderedDict
from config import Config

from inspect import signature


def build_call_args(parameters: OrderedDict, args, kwargs) -> dict:
    """Generate a dict that contains all the positional and keyword args for a function call."""

    arguments = {}
    i = 0
    for parameter in parameters:
        value = None
        if i < len(args):
            value = args[i]
            i += 1

        if parameter in kwargs:
            if value is not None:
                raise ValueError(
                    f"Received both a positional and keyword value for {parameter}"
                ) from None

            value = kwargs[parameter]

        if value is not None:
            arguments[parameter] = value

    return arguments


def feature(feature_name: str, config=Config):
    if not hasattr(config, feature_name):
        raise ValueError(
            f"Unknown feature '{feature_name}'. "
            f"Ensure that {config} contains an attribute for this feature"
        )

    def decorator(function):
        sig = signature(function)

        def wrapper(*args, **kwargs):
            if feature_config := getattr(config, feature_name, None):
                # Don't call function if the feature is not enabled
                if not feature_config["enabled"]:
                    # If None is not a potential return value from the function
                    # this may be problematic
                    return

                arguments = build_call_args(sig.parameters, args, kwargs)

                for key, value in feature_config["default_kwargs"].items():
                    arguments.setdefault(key, value)

                return function(**arguments)

        return wrapper

    return decorator
