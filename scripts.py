#! /usr/bin/env python
""" Scripts for development """

import os
from argparse import ArgumentParser
from subprocess import PIPE, STDOUT, CalledProcessError, run
from typing import Callable, Iterable, Mapping


SOURCE_CODE_LOCATIONS = " ".join(["src", "tests", __file__])
PYPI = os.getenv("PYPI", "pypi")

assert (
    os.getenv("VIRTUAL_ENV") is not None
), "You really should be using a virtual environment"


def _run(command: str, quiet: bool = False) -> str:

    if not quiet:
        print(command)

    try:
        output = run(
            command, check=True, shell=True, text=True, stdout=PIPE, stderr=STDOUT
        ).stdout
    except CalledProcessError as error:
        if not quiet:
            print(error.stdout)
        raise

    if not quiet:
        print(output)
    return output.strip()


def run_black(options: Iterable[str] = ("--check", "--diff")) -> None:
    """Execute black on all source code in project."""

    _run(f"black {' '.join(options)} {SOURCE_CODE_LOCATIONS}")


def run_pylint(options: Iterable[str] = ()) -> None:
    """Execute pylint on all source code in project."""

    _run(f"pylint {' '.join(options)} {SOURCE_CODE_LOCATIONS}")


def run_mypy(options: Iterable[str] = ()) -> None:
    """Execute mypy for all source code in project."""

    _run(f"mypy {' '.join(options)} {SOURCE_CODE_LOCATIONS}")


def static_analysis(tools: Iterable[str] = ("black", "mypy", "pylint")) -> None:
    """Check source code using static analysis"""

    static_analysis_tools = {
        "black": run_black,
        "pylint": run_pylint,
        "mypy": run_mypy,
    }

    for tool in tools:
        static_analysis_tools[tool]()


def test(options: Iterable[str] = ()) -> None:
    """Run all pytest."""

    _run(f"pytest {' '.join(options)}")


COMMANDS: Mapping[str, Callable] = {
    "check": static_analysis,
    "black": run_black,
    "pylint": run_pylint,
    "mypy": run_mypy,
    "test": test,
}

if __name__ == "__main__":
    parser = ArgumentParser(description="Local development scripts")

    parser.add_argument("command", help="command to check", choices=COMMANDS)

    args = parser.parse_args()

    COMMANDS[args.command]()
