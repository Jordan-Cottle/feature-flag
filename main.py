from feature_flag import feature, set_config

from config import Config

set_config(Config)


@feature("greeting")
def greet(message):
    print(message)


@feature("foo")
def foo(spam, count):
    for _ in range(count):
        print(spam)


class Test:
    @feature("bar")
    def bar(self, spam):
        print(spam)


def main():
    greet()
    foo()
    foo("Spam message", count=1)
    foo("Other spam message", 2)

    Test().bar()
    Test().bar("Goodbye world")


if __name__ == "__main__":
    main()
