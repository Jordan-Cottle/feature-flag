"""A simple to use feature flag and config manager that works on a per-function basis."""

from .feature import feature, set_config

__version__ = "0.1.0"
