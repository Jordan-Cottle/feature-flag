""" A feature flag decorator that links configuration input to function calls."""

from functools import wraps
from types import MappingProxyType
from typing import Any, Callable, Mapping, Sequence

from inspect import signature

DEFAULT_CONFIG = None


def set_config(config: Any) -> None:
    """Set config to be used by the feature decorator by default."""

    # I want make this better, I don't like relying on a setter function and this global
    global DEFAULT_CONFIG  # pylint: disable=global-statement

    DEFAULT_CONFIG = config


def build_call_args(
    parameters: MappingProxyType, args: Sequence, kwargs: Mapping
) -> dict:
    """Generate a dict that contains all the positional and keyword args for a function call."""

    arguments = {}
    i = 0
    for parameter in parameters:
        value = None
        if i < len(args):
            value = args[i]
            i += 1

        if parameter in kwargs:
            if value is not None:
                raise ValueError(
                    f"Received both a positional and keyword value for {parameter}"
                ) from None

            value = kwargs[parameter]

        if value is not None:
            arguments[parameter] = value

    return arguments


def get_config(feature_name: str, config: Any) -> Any:
    """Get the configuration for a feature from a config object"""

    try:
        feature_config = config[feature_name]
    except TypeError:  # Handle cases where config is not accessable by string
        feature_config = getattr(config, feature_name, None)

    if not feature_config:
        raise ValueError(
            f"Unknown feature '{feature_name}'. "
            f"Ensure that {config} contains a valid configuration for this feature"
        )

    return feature_config


def feature(feature_name: str, config: Any = None) -> Callable:
    """Register a function as a feature and link it to a configuration.

    If the config marks the function as disabled, then calls to it will become no-ops.

    Default arguments for the function can also be supplied via config instead of hard coded.
    """
    if config is None:
        config = DEFAULT_CONFIG

    def decorator(func: Callable) -> Callable:
        sig = signature(func)

        @wraps(func)
        def wrapper(*args: tuple, **kwargs: dict) -> Any:
            # Lookup config on each call to avoid missing updates to the config object
            # If the config values are updated in place, this is unnecessary, but if
            # the config for a feature is completely replaced then doing this early will
            # not pick up on the updated reference
            feature_config = get_config(feature_name, config)

            # Don't call function if the feature is not enabled
            if not feature_config["enabled"]:
                # If None is not a potential return value from the function
                # this may be problematic
                return None

            # Get a single dict with all arguments as a key:value mapping
            arguments = build_call_args(sig.parameters, args, kwargs)

            for key, value in feature_config.get("default_kwargs").items():
                arguments.setdefault(key, value)

            return func(**arguments)

        return wrapper

    return decorator
