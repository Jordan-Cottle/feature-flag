# Feature Flag

A simple decorator for enabling/disabling functions and supplying default arguments to them via configuration.