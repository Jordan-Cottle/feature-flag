class Config:

    greeting = {
        "enabled": False,
        "default_kwargs": {
            "message": "Hello World",
        },
    }

    foo = {
        "enabled": True,
        "default_kwargs": {
            "spam": "eggs",
            "count": 3,
        },
    }

    bar = {
        "enabled": True,
        "default_kwargs": {
            "spam": "...",
        },
    }
